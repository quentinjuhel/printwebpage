const printBtn = document.querySelector(".print-button");

printBtn.addEventListener('click', function() {
    var pagedPolyfill = document.createElement("script");
    pagedPolyfill.src = "js/paged.polyfill.js";
    document.body.appendChild(pagedPolyfill);

    var pagedCss = document.createElement("link");
    pagedCss.href = "css/pagedjs-interface.css";
    pagedCss.type = "text/css";
    pagedCss.rel = "stylesheet";
    document.head.appendChild(pagedCss);
})